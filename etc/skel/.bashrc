# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h]\$ '

alias ls='ls --color=auto'
alias install='sudo xbps-install -Su'
alias query='sudo xbps-query -Rs'
alias remove='sudo xbps-remove -Ro'
alias services='sudo vsv'
alias update='sudo xbps-install -Su'
alias vxinstaller='sudo void-installer'

set-window-title() {
  echo -en "\033]0;$(pwd)\a"
}

if [[ "$PROMPT_COMMAND" ]]; then
  export PROMPT_COMMAND="$PROMPT_COMMAND;set-window-title"
else
  export PROMPT_COMMAND=set-window-title
fi
